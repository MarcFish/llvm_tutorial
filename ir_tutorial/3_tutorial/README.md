1. Makefile:
   1. `make right`: 为生成test3.cpp对应的ir
   2. `make main`: 为编译main.cpp以获得main, 调用main即可获得利用llvm IR API生成的对应test3.cpp 的汇编文件以及打印对应的ir
   3. `make clean`: 为清除多余文件
2. test3.cpp: 一个示例文件, 主要包含跳转语句
3. main.cpp: 利用llvm IR API来生成test3.cpp相应的ir;
   1. 具体流程在main.cpp以注释形式展示
   2. BasicBlock的构建思路: 将按照test1.cpp中的代码来说明
      1. 首先从主函数第一条语句到if之前是一个BasicBlock, 这个基本块包含了声明, 赋值以及if的条件判断, 从最后一条条件判断来进行跳转.
      2. 然后if语句中分为两个基本块, true对应的基本块以及false对应的基本块.
      3. 最后从完成if语句后, 主函数剩下的语句为最后一个基本块.
   3. 可以重复跳转到同一个基本块(函数的入口基本块不能够被跳转), 但基本块不能间断.
   4. 除了if外, switch, while, for, repeat等均可以用条件跳转以及无条件跳转等实现, 故而不再举例.
