#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"

int main(){
    // 初始化llvm以及设置目标机
    llvm::InitializeNativeTarget();
    llvm::InitializeAllTargetInfos();
    llvm::InitializeAllTargets();
    llvm::InitializeAllTargetMCs();
    llvm::InitializeAllAsmParsers();
    llvm::InitializeAllAsmPrinters();
    // 初始化context以及module
    std::unique_ptr<llvm::LLVMContext> context = std::make_unique<llvm::LLVMContext>();
    std::unique_ptr<llvm::Module> mod = std::make_unique<llvm::Module>("test1.cpp", *context);
    // 获取目标三元组并设置
    auto targetTriple = llvm::sys::getDefaultTargetTriple();
    mod->setTargetTriple(targetTriple);
    // 获取目标机数据类型并设置
    std::string targetError;
    auto target = llvm::TargetRegistry::lookupTarget(targetTriple, targetError);
    auto cpu = "generic";
    auto features = "";
    llvm::TargetOptions options;
    auto relocationModel = llvm::Reloc::Model::PIC_;
    auto theTargetMachine = target->createTargetMachine(targetTriple, cpu, features, options, relocationModel);
    mod->setDataLayout(theTargetMachine->createDataLayout());
    // 创建主函数以及builder, 需要使用的block
    auto mainFunctionType = llvm::FunctionType::get(llvm::Type::getInt32Ty(*context), false);
    auto mainFunction = llvm::Function::Create(mainFunctionType, llvm::GlobalValue::ExternalLinkage, "main", mod.get());
    auto entryBlock = llvm::BasicBlock::Create(*context, "", mainFunction);  //入口block
    auto thenBlock = llvm::BasicBlock::Create(*context, "", mainFunction);  // ifthen block
    auto elseBlock = llvm::BasicBlock::Create(*context, "", mainFunction);  // else block
    auto mainBlock1 = llvm::BasicBlock::Create(*context, "", mainFunction);  // ifelse后的block
    std::unique_ptr<llvm::IRBuilder<>> builder = std::make_unique<llvm::IRBuilder<>>(entryBlock);
    // 在栈上分配两个int32大小的空间
    auto addr1 = builder->CreateAlloca(llvm::Type::getInt32Ty(*context), nullptr);  // int a
    auto addr2 = builder->CreateAlloca(llvm::Type::getInt32Ty(*context), nullptr);  // int b
    // 获取常量
    auto value1 = llvm::ConstantInt::get(llvm::Type::getInt32Ty(*context), 3);
    auto value2 = llvm::ConstantInt::get(llvm::Type::getInt32Ty(*context), 5);
    auto value3 = llvm::ConstantInt::get(llvm::Type::getInt32Ty(*context), 9);
    // 将常量存储进 上面分配的空间
    builder->CreateStore(value1, addr1);  // a=3
    // 从上面分配的空间中取出值
    auto value4 = builder->CreateLoad(llvm::Type::getInt32Ty(*context), addr1);
    // 将取出的值与常量进行比较
    auto value5 = builder->CreateICmpEQ(value4, value1);  // a==3?
    builder->CreateCondBr(value5, thenBlock, elseBlock); // 创建条件跳转指令
    // 插入thenBlock指令
    builder->SetInsertPoint(thenBlock);
    builder->CreateStore(value2, addr2);  // b=5
    builder->CreateBr(mainBlock1);  // 无条件跳转
    // 插入elseBlock指令
    builder->SetInsertPoint(elseBlock);
    builder->CreateStore(value3, addr2);  // b=9
    builder->CreateBr(mainBlock1);
    // 插入mainBlock1指令
    builder->SetInsertPoint(mainBlock1);
    auto value6 = builder->CreateLoad(llvm::Type::getInt32Ty(*context), addr2);
    builder->CreateRet(value6);  // return b
    // 输出汇编文件
    std::error_code errorCode;
    std::string filename = "main.s";  // 汇编文件名
    llvm::raw_fd_ostream dest(filename, errorCode, llvm::sys::fs::OF_None);
    llvm::legacy::PassManager pass;
    llvm::CodeGenFileType type = llvm::CGFT_AssemblyFile;  // 文件类型为汇编
    // llvm::CodeGenFileType type = llvm::CGFT_ObjectFile;  // 文件类型为可执行文件
    theTargetMachine->addPassesToEmitFile(pass, dest, nullptr, type);
    pass.run(*mod);
    dest.flush();
    mod->print(llvm::outs(), nullptr);  // 打印ir
}
