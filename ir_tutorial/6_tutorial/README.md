1. Makefile:
   1. `make right`: 为生成test6.cpp对应的ir
   2. `make main`: 为编译main.cpp以获得main, 调用main即可获得利用llvm IR API生成的对应test6.cpp 的汇编文件以及打印对应的ir
   3. `make clean`: 为清除多余文件
2. test6.cpp: 一个示例文件, 主要包含printf的声明以及使用
3. main.cpp: 利用llvm IR API来生成test6.cpp相应的ir;
   1. 具体流程在main.cpp以注释形式展示
   2. 在makefile中增加了链接库
   3. printf函数的构造与c中一致即可.
   4. IO函数的实现的另一种方法是通过链接的方式.
