1. 教程简介: 
   1. 本教程展示了利用LLVM IR Builder创建LLVM IR的一些基础使用方法. 
   2. 本教程暂不涉及具体的IR指令讲解以及原生IR API的使用.
   3. 在子目录中会有相应教程的说明
2. 环境
   1. LLVM 10.0.0
   2. Ubuntu 20.04
2. 教程说明
   1. 1_tutorial: 声明变量, 相加, 创建主函数以及函数返回
   2. 2_tutorial: 函数声明以及调用
   3. 3_tutorial: 跳转语句
   4. 4_tutorial: 结构体声明, 使用以及类型转换
   5. 5_tutorial: 数组声明以及使用
   6. 6_tutorial: printf的声明以及使用
