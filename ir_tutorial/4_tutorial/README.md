1. Makefile:
   1. `make right`: 为生成test4.cpp对应的ir
   2. `make main`: 为编译main.cpp以获得main, 调用main即可获得利用llvm IR API生成的对应test4.cpp 的汇编文件以及打印对应的ir
   3. `make clean`: 为清除多余文件
2. test4.cpp: 一个示例文件, 主要包含结构体以及隐式转换
3. main.cpp: 利用llvm IR API来生成test4.cpp相应的ir;
   1. 具体流程在main.cpp以注释形式展示
   2. gep指令说明
      1. gep指令是计算地址而不进行地址访问
      2. gep输入一个基地址类型以及基地址, 然后计算基地址偏移后的地址偏移. 即gep有两个偏移量, 第一个偏移量为基地址偏移量, 第二个偏移量为计算完基地址后在此基础上进行的偏移.
      3. 以c中的结构体为例, `struct test {int a;int b'} *x;`
         1. `x`为一个结构体指针, 即基地址. 
         2. `x[0]`为在基地址基础上偏移量为0的地址
         3. `x[0].a`为在上述地址上访问字段a. 相应在gep中为0, 即x的第一个字段.
