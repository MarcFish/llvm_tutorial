1. Makefile:
   1. `make right`: 为生成test2.cpp对应的ir
   2. `make main`: 为编译main.cpp以获得main, 调用main即可获得利用llvm IR API生成的对应test2.cpp 的汇编文件以及打印对应的ir
   3. `make clean`: 为清除多余文件
2. test2.cpp: 一个示例文件, 主要包含函数声明以及调用
3. main.cpp: 利用llvm IR API来生成test2.cpp相应的ir;
   1. 具体流程在main.cpp以注释形式展示
