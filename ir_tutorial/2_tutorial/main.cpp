#include <vector>

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"

int main(){
    // 初始化llvm以及设置目标机
    llvm::InitializeNativeTarget();
    llvm::InitializeAllTargetInfos();
    llvm::InitializeAllTargets();
    llvm::InitializeAllTargetMCs();
    llvm::InitializeAllAsmParsers();
    llvm::InitializeAllAsmPrinters();
    // 初始化context以及module
    std::unique_ptr<llvm::LLVMContext> context = std::make_unique<llvm::LLVMContext>();
    std::unique_ptr<llvm::Module> mod = std::make_unique<llvm::Module>("test1.cpp", *context);
    // 获取目标三元组并设置
    auto targetTriple = llvm::sys::getDefaultTargetTriple();
    mod->setTargetTriple(targetTriple);
    // 获取目标机数据类型并设置
    std::string targetError;
    auto target = llvm::TargetRegistry::lookupTarget(targetTriple, targetError);
    auto cpu = "generic";
    auto features = "";
    llvm::TargetOptions options;
    auto relocationModel = llvm::Reloc::Model::PIC_;
    auto theTargetMachine = target->createTargetMachine(targetTriple, cpu, features, options, relocationModel);
    mod->setDataLayout(theTargetMachine->createDataLayout());
    // 创建sum函数
    llvm::SmallVector<llvm::Type *, 2> sumFunctionArgs;  // sum函数的参数
    sumFunctionArgs.push_back(llvm::Type::getInt32Ty(*context));
    sumFunctionArgs.push_back(llvm::Type::getInt32Ty(*context));
    auto sumFunctionType = llvm::FunctionType::get(llvm::Type::getInt32Ty(*context), sumFunctionArgs, false);
    auto sumFunction = llvm::Function::Create(sumFunctionType, llvm::GlobalValue::ExternalLinkage, "sum", mod.get());
    auto sumBlock = llvm::BasicBlock::Create(*context, "", sumFunction);  // sum函数的基本块
    std::unique_ptr<llvm::IRBuilder<>> builder = std::make_unique<llvm::IRBuilder<>>(sumBlock);
    auto sumargs = sumFunction->arg_begin();  // 获取sum的参数
    auto sumValue1 = sumargs++;
    auto sumValue2 = sumargs++;
    auto sumValue3 = builder->CreateAdd(sumValue1, sumValue2);
    builder->CreateRet(sumValue3);  // sum的返回
    // 创建主函数以及builder
    auto mainFunctionType = llvm::FunctionType::get(llvm::Type::getInt32Ty(*context), false);
    auto mainFunction = llvm::Function::Create(mainFunctionType, llvm::GlobalValue::ExternalLinkage, "main", mod.get());
    auto entryBlock = llvm::BasicBlock::Create(*context, "", mainFunction);
    builder->SetInsertPoint(entryBlock);  // 将builder插入位置放到main函数的基本块
    // 在栈上分配一个int32大小的空间
    auto addr1 = builder->CreateAlloca(llvm::Type::getInt32Ty(*context), nullptr);
    // 获取两个常量
    auto value1 = llvm::ConstantInt::get(llvm::Type::getInt32Ty(*context), 3);
    auto value2 = llvm::ConstantInt::get(llvm::Type::getInt32Ty(*context), 5);
    // 将常量存储进 上面分配的空间
    builder->CreateStore(value1, addr1);
    // 从上面分配的空间中取出值
    auto value3 = builder->CreateLoad(llvm::Type::getInt32Ty(*context), addr1);
    // 将取出的值组成为vector的形式以作为参数传递进入sum函数中
    std::vector<llvm::Value*> args;
    args.push_back(value2);
    args.push_back(value3);
    auto value5 = builder->CreateCall(sumFunction, args);  // 创建一条调用指令
    // 将运算结果返回
    builder->CreateRet(value5);
    // 输出汇编文件
    std::error_code errorCode;
    std::string filename = "main.s";  // 汇编文件名
    llvm::raw_fd_ostream dest(filename, errorCode, llvm::sys::fs::OF_None);
    llvm::legacy::PassManager pass;
    llvm::CodeGenFileType type = llvm::CGFT_AssemblyFile;  // 文件类型为汇编
    // llvm::CodeGenFileType type = llvm::CGFT_ObjectFile;  // 文件类型为可执行文件
    theTargetMachine->addPassesToEmitFile(pass, dest, nullptr, type);
    pass.run(*mod);
    dest.flush();
    mod->print(llvm::outs(), nullptr);  // 打印ir
}
