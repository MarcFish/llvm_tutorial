1. Makefile:
   1. `make right`: 为生成test1.cpp对应的ir
   2. `make main`: 为编译main.cpp以获得main, 调用main即可获得利用llvm IR API生成的对应test1.cpp 的汇编文件以及打印对应的ir
   3. `make clean`: 为清除多余文件
2. test1.cpp: 一个示例文件, 主要包含声明变量, 相加, 函数返回操作.
3. main.cpp: 利用llvm IR API来生成test1.cpp相应的ir;
   1. 具体流程在main.cpp以注释形式展示
